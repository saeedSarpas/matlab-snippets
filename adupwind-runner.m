% Resolution
N = 100;

% Domain
x = linspace(0,1,N+2); % +2 for ghost cells
dx = x(3:end) - x(2:end-1);

% IC: Sinusoid
u_int = -1/(2*pi)*cos(2*pi*x);
u = (u_int(3:end) - u_int(2:end-1)) ./ dx;

% time step
dt = 0.01;

for t = 0:dt:3
    % ODE solver
    [t, u] = ode45(@adupwind, [0,dt], u);
    u = u(end,:);
    
    % BC: Periodic
    u(1) = u(end-1);
    u(end) = u(2);
    
    plot([x(1:end-2); x(2:end-1)], [u; u], 'linewidth', 7);
    ylim([-2.1,2.1]);
    
    drawnow;
    pause(dt);
end
